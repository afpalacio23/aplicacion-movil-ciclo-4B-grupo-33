import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';

class LoginPage extends StatelessWidget {
  final _logoUrl = 'assets/images/Logo.png';
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              _logo(),
              _iniciarSesion(),
              _registroSesion(),
              _omitir(),
            ],
          ),
        ),
      ),
    );
  }

  // Logo inicial aplicacion
  Widget _logo() {
    return Column(
      children: [
        const SizedBox(
          height: 120,
        ),
        Center(
          child: Image.asset(_logoUrl),
        ),
        const SizedBox(
          height: 50,
        )
      ],
    );
  }

  // inicio de Sesion
  Widget _iniciarSesion() {
    return Column(
      children: [
        const Text(
          'Iniciar sesión con:',
          style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.w800,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        _login(),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }

  // Registro con
  Widget _registroSesion() {
    return Column(
      children: [
        const SizedBox(
          height: 16,
        ),
        const Text(
          'Registrarse con:',
          style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.w800,
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        _signUp(),
      ],
    );
  }

  // Omitir
  Widget _omitir() {
    return Column(
      children: [
        const SizedBox(
          height: 150,
        ),
        TextButton(
          onPressed: () {},
          child: const Text(
            'Omitir',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w800,
            ),
          ),
        )
      ],
    );
  }

  // Boton inicio de sesion
  Widget _login() {
    return SignInButton(
      Buttons.Google,
      text: 'Login with Google',
      onPressed: () {},
    );
  }

  //Boton registro
  Widget _signUp() {
    return SignInButton(
      Buttons.Google,
      text: 'Sign up with Google',
      onPressed: () {},
    );
  }
}
