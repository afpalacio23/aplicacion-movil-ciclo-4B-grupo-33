import 'package:flutter/material.dart';

class CreacionPerfil extends StatelessWidget {
  const CreacionPerfil({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Creacion de Perfil",
          style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.w800,
          ),
        ),
        centerTitle: true,
        leading: GestureDetector(
          onTap: () {/* Write listener code here */},
          child: const Icon(
            Icons.arrow_back_rounded,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 16,
              ),
              const SizedBox(
                height: 16,
                //Posicion de formlacio
              ),
              _formulario(context)
            ],
          ),
        ),
      ),
    );
  }

  //Estructura de formario formalario
  Widget _formulario(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return Form(
      key: formKey,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Column(
          children: [
            const Text(
              "Nombre",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            _formNombre(),
            const SizedBox(
              height: 16,
            ),
            const Text(
              "Apellido",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            _formaApellido(),
            const SizedBox(
              height: 16,
            ),
            const Text(
              "Altura",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            _formAltura(),
            const SizedBox(
              height: 16,
            ),
            const Text(
              "Peso",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            _formPeso(),
            const SizedBox(
              height: 16,
            ),
            const Text(
              "Ingesta Calorica Diaria",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            _formIngesCaloricaDiaria(),
            const SizedBox(
              height: 25,
            ),
            _botonCrearPerfil()
          ],
        ),
      ),
    );
  }

  // Nombre
  Widget _formNombre() {
    return TextFormField(
      autofocus: true,
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        filled: true,
        //floatingLabelBehavior: FloatingLabelBehavior.never,
        // Agregar el color del fill con codigo hex
        border: OutlineInputBorder(
          borderSide: BorderSide.none,

          //borderRadius: BorderRadius.circular(10),
        ),
        hintText: 'Nombre',
      ),
      textAlign: TextAlign.center,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Debes identificarte';
        }
        return null;
      },
    );
  }

  // Apellido
  Widget _formaApellido() {
    return TextFormField(
      autofocus: true,
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        filled: true,
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          //borderRadius: BorderRadius.circular(10),
        ),
        hintText: 'Apellido',
      ),
      textAlign: TextAlign.center,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Campo Obligatorio';
        }
        return null;
      },
    );
  }

  // Altura
  Widget _formAltura() {
    return TextFormField(
      autofocus: true,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        filled: true,
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          //borderRadius: BorderRadius.circular(10),
        ),
        hintText: 'Altura',
      ),
      textAlign: TextAlign.center,
      // Validar el contenido dentro de la
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Campo Obligatorio';
        }
        return null;
      },
    );
  }

  // Peso
  Widget _formPeso() {
    return TextFormField(
      autofocus: true,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        filled: true,
        //floatingLabelBehavior: FloatingLabelBehavior.never,
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          //borderRadius: BorderRadius.circular(10),
        ),
        hintText: 'Peso',
      ),
      textAlign: TextAlign.center,
      // Validar el contenido dentro de la
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Campo Obligatorio';
        }
        return null;
      },
    );
  }

  // Ingesta Caloriaca Diaria
  Widget _formIngesCaloricaDiaria() {
    return TextFormField(
      autofocus: true,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        filled: true,
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          //borderRadius: BorderRadius.circular(10),
        ),
        hintText: 'Ingesta Calorica',
      ),
      textAlign: TextAlign.center,
      // Validar el contenido dentro de la
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Campo Obligatorio';
        }
        return null;
      },
    );
  }

  //Boton summit
  Widget _botonCrearPerfil() {
    return Column(
      children: [
        ElevatedButton(
          child: const Text(
            'Guardar',
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          onPressed: () {
            // Aqui se pone el comportamiento del boton
          },
        )
      ],
    );
  }
}
